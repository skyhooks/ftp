﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FtpDesktop
{
    public struct SafeDelay
    {
        public int HighDef { get; set; }
        public int StandardDef { get; set; }
    }
}
