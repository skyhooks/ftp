﻿using FluentFTP;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace FtpDesktop
{
    static class Utils
    {
        public static (string ftpUrl, string ftpUsername, string ftpPassword, Config config) GetConfiguration()
        {
            NameValueCollection appSettings = ConfigurationManager.AppSettings;

            Config config = new Config
            {
                DownloadFolder = appSettings["DownloadFolder"],
                ScanTimer = int.Parse(appSettings["Timer"]),
                SafeFilesOnly = bool.Parse(appSettings["SafeFilesOnly"])
            };
            string username = appSettings["FtpUserName"];
            string password = appSettings["FtpPassword"];
            string url = appSettings["FtpUrl"];

            SafeDelay safeDelay = new SafeDelay();

            int delay;
            if (int.TryParse(appSettings["SafeFileDelaySD"], out delay))
            {
                safeDelay.StandardDef = delay;
            }
            else
            {
                safeDelay.StandardDef = 30;
            }

            if (int.TryParse(appSettings["SafeFileDelayHD"], out delay))
            {
                safeDelay.HighDef = delay;
            }
            else
            {
                safeDelay.HighDef = 15;
            }

            config.SafeDelay = safeDelay;

            return (url, username, password, config);
        }

        public static bool SaveRemoteListing(List<RecordedProgramInfo> fileList, string filename)
        {
            if (fileList != null)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<RecordedProgramInfo>));
                    TextWriter writer = new StreamWriter(filename);
                    serializer.Serialize(writer, fileList);
                    writer.Close();

                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        public static List<RecordedProgramInfo> LoadRemoteListing(string filename)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<RecordedProgramInfo>));
                FileStream stream = new FileStream(filename, FileMode.Open);
                return (List<RecordedProgramInfo>)serializer.Deserialize(stream);
            }
            catch
            {
                return null;
            }
        }
    }
}
