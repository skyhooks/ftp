﻿using FluentFTP;
using System;

namespace FtpDesktop
{
    [Serializable()]
    public class RecordedProgramInfo
    {
        string _fileName;
        string _programmeName;
        string _tvChannel;
        string _fileExtension;
        DateTime _recordedDate;
        DateTime _modifiedDate;
        long _fileSizeB;

        private RecordedProgramInfo() { }

        public RecordedProgramInfo(FtpListItem item)
        {
            _fileName = item.Name;
            _modifiedDate = item.Modified;
            _fileSizeB = item.Size;

            DecodeFilename(_fileName);
        }

        public string FileName { get { return _fileName; } set { _fileName = value.Trim(); } }
        public string ProgrammeName{ get{ return _programmeName; } set { _programmeName = value; } }
        public string TvChannel { get { return _tvChannel; } set { _tvChannel = value; } }
        public string FileExtension { get { return _fileExtension; } set { _fileExtension = value; } }
        public DateTime RecordedDate { get { return _recordedDate; } set { _recordedDate = value; } }
        public DateTime ModifiedDate { get { return _modifiedDate; } set { _modifiedDate = value; } }
        public bool IsHighDef { get { return _tvChannel.EndsWith("HD"); } }
        public long FileSizeB { get { return _fileSizeB; } set { _fileSizeB = value; } }
        public long FileSizeKB { get { return _fileSizeB / 1024; } }
        public long FileSizeMB { get { return _fileSizeB / 1024 / 1024; } }

        private void DecodeFilename(string data)
        {
            if (data.IndexOf(".") > 1)
            {
                _fileExtension = data.Substring(data.LastIndexOf(".") + 1);

                if (_fileExtension == "wtv")
                {
                    // FILE NAME WITH SPACES_Channel_YYYY_MM_DD_HH_MM_SS.wtv
                    ProgrammeName = data.Substring(0, data.IndexOf("_"));

                    data = data.Substring(_programmeName.Length + 1);

                    TvChannel = data.Substring(0, data.IndexOf("_"));

                    data = data.Substring(_tvChannel.Length + 1);

                    // YYYY_MM_DD_HH_MM_SS
                    int year = int.Parse(data.Substring(0, 4));
                    int month = int.Parse(data.Substring(5, 2));
                    int day = int.Parse(data.Substring(8, 2));
                    int hours = int.Parse(data.Substring(11, 2));
                    int minute = int.Parse(data.Substring(14, 2));
                    int second = int.Parse(data.Substring(17, 2));
                    RecordedDate = new DateTime(year, month, day, hours, minute, second);
                }
            }
        }

        public override string ToString()
        {
            return $"{RecordedDate}\t{FileSizeMB} Mb\t\t{ProgrammeName}";
        }
    }
}
