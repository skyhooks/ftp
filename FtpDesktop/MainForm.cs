﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FtpDesktop
{
    public partial class MainForm : Form
    {
        BindingSource _bindingSource;
        FtpUtils _ftpUtils;
        Config _config;
        string _previousMessage = string.Empty;

        #region Form Initialisation
        public MainForm()
        {
            InitializeComponent();

            dataGridViewListings.AutoGenerateColumns = false;

            var config = Utils.GetConfiguration();

            _config = config.config;
            _config.LogFileFolder = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            _ftpUtils = new FtpUtils(config.ftpUrl, config.ftpUsername, config.ftpPassword);

            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1 && args[1] == "AutoStart")
            {
                ToggleTimer();
            }

            numericUpDownInterval.Value = _config.ScanTimer;
            labelHD.Text = $"{_config.SafeDelay.HighDef.ToString()} mins";
            labelSD.Text = $"{_config.SafeDelay.StandardDef.ToString()} mins";
            checkBoxSafeFilesOnly.Checked = _config.SafeFilesOnly;
        }
        #endregion

        #region Event Handlers
        private void buttonGetListing_Click(object sender, EventArgs e)
        {
            DownloadListing();
        }

        private void checkBoxSafeFilesOnly_CheckedChanged(object sender, EventArgs e)
        {
            _config.SafeFilesOnly = checkBoxSafeFilesOnly.Checked;
        }

        private void dataGridViewListings_DoubleClick(object sender, EventArgs e)
        {
            DownloadSelectedItem();
        }

        private void buttonDownload_Click(object sender, EventArgs e)
        {
            DownloadSelectedItem();
        }

        private void buttonDownloadAll_Click(object sender, EventArgs e)
        {
            DownloadAllFiles();
        }

        private void buttonToggleTimer_Click(object sender, EventArgs e)
        {
            ToggleTimer();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Enabled = false;
            try
            {
                ScanForChanges();
            }
            finally
            {
                timer.Enabled = true;
            }
        }

        private void numericUpDownInterval_ValueChanged(object sender, EventArgs e)
        {
            _config.ScanTimer = (int)numericUpDownInterval.Value;
        }
        #endregion

        #region Private Methods

        private void ToggleTimer()
        {
            SetControlState(timer.Enabled);
            Refresh();

            if (timer.Enabled)
            {
                timer.Enabled = false;
            }
            else
            {
                ScanForChanges();
                timer.Interval = _config.ScanTimerMilliseconds;
                timer.Enabled = true;
            }
        }

        private void SetControlState(bool enabled)
        {
            foreach(Control control in panelButtons.Controls)
            {
                if (control is Button || 
                    control is CheckBox || 
                    control is NumericUpDown)
                {
                    control.Enabled = enabled || control.Name == "buttonToggleTimer";
                }
            }
            buttonToggleTimer.Text = enabled ? "Start Timer" : "Stop Timer";
        }

        private void ScanForChanges()
        {
            if(DownloadListing())
            {
                DownloadAllFiles();
            }
        }

        private bool DownloadListing()
        {
            string message = string.Empty;
            _bindingSource = _ftpUtils.GetRemoteListing(ref message);
            dataGridViewListings.DataSource = _bindingSource;

            if (message != string.Empty)
            {
                AddStatus($"Unable to retrieve listings, the error was: {message}");
                return false;
            }

            dataGridViewListings.Refresh();

            return true;
        }

        private List<RecordedProgramInfo> AlternateDownloadSelected()
        {
            List<RecordedProgramInfo> list = new List<RecordedProgramInfo>
            {
                (RecordedProgramInfo)dataGridViewListings.SelectedRows[0].DataBoundItem
            };
            return list;
        }

        private List<RecordedProgramInfo> AlternateDownloadAll()
        {
            List<RecordedProgramInfo> list = new List<RecordedProgramInfo>();
            foreach (object item in _bindingSource)
            {
                list.Add((RecordedProgramInfo)item);
            }
            return list;
        }

        private void DownloadSelectedItem()
        {
            string errorMessage = string.Empty;
            if (dataGridViewListings.SelectedRows.Count == 1)
            {
                RecordedProgramInfo recordedProgramInfo = (RecordedProgramInfo)dataGridViewListings.SelectedRows[0].DataBoundItem;
                AddStatus($"Downloading {recordedProgramInfo.ProgrammeName}");

                try
                {
                    DateTime downloadStartTime = DateTime.Now;
                    if (_ftpUtils.DownloadFile(recordedProgramInfo.FileName, recordedProgramInfo.FileSizeB, $"{_config.DownloadFolder}", ref errorMessage))
                    {
                        string downloadDurationMessage = CreateDurationMessage(downloadStartTime, DateTime.Now);
                        AddStatus($"Download of {recordedProgramInfo.ProgrammeName} completed in {downloadDurationMessage}");

                        if (!_ftpUtils.MoveFile(recordedProgramInfo.FileName, "Archive", ref errorMessage))
                        {
                            UpdateStatus($": Error moving file: {errorMessage}");
                        }
                    }
                    else
                    {

                        if (errorMessage == string.Empty)
                        {
                            AddStatus($"Download of {recordedProgramInfo.ProgrammeName} failed without an error.");
                        }
                        else
                        {
                            AddStatus($"Download of {recordedProgramInfo.ProgrammeName} failed with the following error: {errorMessage}");
                        }
                    }
                }
                catch (Exception e)
                {
                    AddStatus($"Download of {recordedProgramInfo.ProgrammeName} failed with the following error: {e.Message}");
                }
            }
        }

        private void DownloadAllFiles()
        {
            string errorMessage = string.Empty;
            int filesDownloaded = 0;
            int filesSkipped = 0;
            DateTime processStartTime = DateTime.Now;

            foreach (object item in _bindingSource)
            {
                RecordedProgramInfo recordedProgramInfo = (RecordedProgramInfo)item;

                AddStatus($"Processing {recordedProgramInfo.ProgrammeName}");

                if (_config.SafeFilesOnly && (DateTime.Now - recordedProgramInfo.ModifiedDate).TotalMinutes < (recordedProgramInfo.IsHighDef ? _config.SafeDelay.HighDef : _config.SafeDelay.StandardDef))
                {
                    filesSkipped++;
                    ReplaceStatus($"Too early to download {recordedProgramInfo.ProgrammeName}");
                    continue;
                }

                try
                {
                    DateTime downloadStartTime = DateTime.Now;

                    ReplaceStatus($"Downloading {recordedProgramInfo.ProgrammeName}");
                    if (_ftpUtils.DownloadFile(recordedProgramInfo.FileName, recordedProgramInfo.FileSizeB, $@"{_config.DownloadFolder}", ref errorMessage))
                    {
                        ReplaceStatus($"Downloaded {recordedProgramInfo.ProgrammeName} in {CreateDurationMessage(downloadStartTime, DateTime.Now)}");

                        if (!_ftpUtils.MoveFile(recordedProgramInfo.FileName, "Archive", ref errorMessage))
                        {
                            UpdateStatus($": Error moving file: {errorMessage}");
                        }
                        filesDownloaded++;
                    }
                    else
                    {
                        if (errorMessage == string.Empty)
                        {
                            AddStatus($"Download of {recordedProgramInfo.ProgrammeName} failed without an error.");
                        }
                        else
                        {
                            AddStatus($"Download of {recordedProgramInfo.ProgrammeName} failed with the following error: {errorMessage}");
                        }
                    }
                }
                catch (Exception e)
                {
                    AddStatus($"Download of {recordedProgramInfo.ProgrammeName} failed with the following error: {e.Message}");
                }
            }

            if (filesSkipped == 0 && filesDownloaded == 0)
            {
                AddStatus($"Checked for programmes to download and none found");
            }
            else if (filesSkipped > 0 && filesDownloaded > 0)
            {
                AddStatus($"Downloaded {filesDownloaded} programmes in {CreateDurationMessage(processStartTime, DateTime.Now)}, skipped {filesSkipped} programmes.");
                AddStatus(new String('-', 100));
            }
            else if (filesDownloaded > 0)
            {
                AddStatus($"Downloaded {filesDownloaded} programmes in {CreateDurationMessage(processStartTime, DateTime.Now)}.");
                AddStatus(new String('-', 100));
            }
            else
            {
                AddStatus($"Skipped {filesSkipped} programmes.");
                AddStatus(new String('-', 100));
            }
        }

        private string CreateDurationMessage(DateTime downloadStartTime, DateTime downloadEndTime)
        {
            string downloadDurationMessage;
            TimeSpan downloadDuration = downloadEndTime - downloadStartTime;
            if (downloadDuration.Hours > 0)
            {
                downloadDurationMessage = $"{downloadDuration.Hours} hours {downloadDuration.Minutes} minutes and {downloadDuration.Seconds} seconds";
            }
            else if (downloadDuration.Minutes > 0)
            {
                downloadDurationMessage = $"{downloadDuration.Minutes} minutes {downloadDuration.Seconds} seconds";
            }
            else
            {
                downloadDurationMessage = $"{downloadDuration.Seconds} seconds";
            }
            return downloadDurationMessage;
        }

        private void AddStatus(string message)
        {
            if (message == _previousMessage)
            {
                ReplaceStatus(message);
                return;
            }
            listBoxStatus.Items.Insert(0, $"{DateTime.Now} {message}");
            listBoxStatus.Refresh();
            _previousMessage = message;
        }

        private void UpdateStatus(string message)
        {
            listBoxStatus.Items[0] = $"{listBoxStatus.Items[0]} {message}";
            listBoxStatus.Refresh();
        }

        private void ReplaceStatus(string message)
        {
            listBoxStatus.Items[0] = $"{DateTime.Now} {message}";
            listBoxStatus.Refresh();
        }
        #endregion
    }
}
