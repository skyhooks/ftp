﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FtpDesktop
{
    public class Config
    {
        public string DownloadFolder { get; set; }
        public string LogFileFolder { get; set; }
        public bool SafeFilesOnly { get; set; }
        public SafeDelay SafeDelay { get; set; }
        public int ScanTimer { get; set; }
        public int ScanTimerMilliseconds
        {
            get
            {
                return ScanTimer * 1000 * 60;
            }
        }
    }
}
