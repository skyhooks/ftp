﻿namespace FtpDesktop
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelButtons = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownInterval = new System.Windows.Forms.NumericUpDown();
            this.buttonDownload = new System.Windows.Forms.Button();
            this.buttonToggleTimer = new System.Windows.Forms.Button();
            this.buttonGetListing = new System.Windows.Forms.Button();
            this.buttonDownloadAll = new System.Windows.Forms.Button();
            this.checkBoxSafeFilesOnly = new System.Windows.Forms.CheckBox();
            this.panelOutput = new System.Windows.Forms.Panel();
            this.listBoxStatus = new System.Windows.Forms.ListBox();
            this.panelListings = new System.Windows.Forms.Panel();
            this.dataGridViewListings = new System.Windows.Forms.DataGridView();
            this.RecordedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModifiedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProgrammeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Channel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsHighDef = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Filesize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelHD = new System.Windows.Forms.Label();
            this.labelSD = new System.Windows.Forms.Label();
            this.panelButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownInterval)).BeginInit();
            this.panelOutput.SuspendLayout();
            this.panelListings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListings)).BeginInit();
            this.SuspendLayout();
            // 
            // panelButtons
            // 
            this.panelButtons.Controls.Add(this.labelSD);
            this.panelButtons.Controls.Add(this.labelHD);
            this.panelButtons.Controls.Add(this.label4);
            this.panelButtons.Controls.Add(this.label3);
            this.panelButtons.Controls.Add(this.label2);
            this.panelButtons.Controls.Add(this.label1);
            this.panelButtons.Controls.Add(this.numericUpDownInterval);
            this.panelButtons.Controls.Add(this.buttonDownload);
            this.panelButtons.Controls.Add(this.buttonToggleTimer);
            this.panelButtons.Controls.Add(this.buttonGetListing);
            this.panelButtons.Controls.Add(this.buttonDownloadAll);
            this.panelButtons.Controls.Add(this.checkBoxSafeFilesOnly);
            this.panelButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelButtons.Location = new System.Drawing.Point(0, 0);
            this.panelButtons.Name = "panelButtons";
            this.panelButtons.Size = new System.Drawing.Size(1120, 67);
            this.panelButtons.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(912, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 19;
            this.label2.Text = "minutes";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(738, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 20);
            this.label1.TabIndex = 18;
            this.label1.Text = "Timer interval";
            // 
            // numericUpDownInterval
            // 
            this.numericUpDownInterval.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownInterval.Location = new System.Drawing.Point(844, 22);
            this.numericUpDownInterval.Name = "numericUpDownInterval";
            this.numericUpDownInterval.Size = new System.Drawing.Size(62, 26);
            this.numericUpDownInterval.TabIndex = 17;
            this.numericUpDownInterval.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownInterval.ValueChanged += new System.EventHandler(this.numericUpDownInterval_ValueChanged);
            // 
            // buttonDownload
            // 
            this.buttonDownload.Location = new System.Drawing.Point(181, 16);
            this.buttonDownload.Name = "buttonDownload";
            this.buttonDownload.Size = new System.Drawing.Size(119, 37);
            this.buttonDownload.TabIndex = 13;
            this.buttonDownload.Text = "Download";
            this.buttonDownload.UseVisualStyleBackColor = true;
            this.buttonDownload.Click += new System.EventHandler(this.buttonDownload_Click);
            // 
            // buttonToggleTimer
            // 
            this.buttonToggleTimer.Location = new System.Drawing.Point(431, 16);
            this.buttonToggleTimer.Name = "buttonToggleTimer";
            this.buttonToggleTimer.Size = new System.Drawing.Size(119, 37);
            this.buttonToggleTimer.TabIndex = 16;
            this.buttonToggleTimer.Text = "Start Timer";
            this.buttonToggleTimer.UseVisualStyleBackColor = true;
            this.buttonToggleTimer.Click += new System.EventHandler(this.buttonToggleTimer_Click);
            // 
            // buttonGetListing
            // 
            this.buttonGetListing.Location = new System.Drawing.Point(12, 16);
            this.buttonGetListing.Name = "buttonGetListing";
            this.buttonGetListing.Size = new System.Drawing.Size(163, 37);
            this.buttonGetListing.TabIndex = 9;
            this.buttonGetListing.Text = "Get Listings";
            this.buttonGetListing.UseVisualStyleBackColor = true;
            this.buttonGetListing.Click += new System.EventHandler(this.buttonGetListing_Click);
            // 
            // buttonDownloadAll
            // 
            this.buttonDownloadAll.Location = new System.Drawing.Point(306, 16);
            this.buttonDownloadAll.Name = "buttonDownloadAll";
            this.buttonDownloadAll.Size = new System.Drawing.Size(119, 37);
            this.buttonDownloadAll.TabIndex = 14;
            this.buttonDownloadAll.Text = "Download All";
            this.buttonDownloadAll.UseVisualStyleBackColor = true;
            this.buttonDownloadAll.Click += new System.EventHandler(this.buttonDownloadAll_Click);
            // 
            // checkBoxSafeFilesOnly
            // 
            this.checkBoxSafeFilesOnly.AutoSize = true;
            this.checkBoxSafeFilesOnly.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxSafeFilesOnly.Checked = true;
            this.checkBoxSafeFilesOnly.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSafeFilesOnly.Location = new System.Drawing.Point(586, 23);
            this.checkBoxSafeFilesOnly.Name = "checkBoxSafeFilesOnly";
            this.checkBoxSafeFilesOnly.Size = new System.Drawing.Size(141, 24);
            this.checkBoxSafeFilesOnly.TabIndex = 15;
            this.checkBoxSafeFilesOnly.Text = "Safe Files Only";
            this.checkBoxSafeFilesOnly.UseVisualStyleBackColor = true;
            this.checkBoxSafeFilesOnly.CheckedChanged += new System.EventHandler(this.checkBoxSafeFilesOnly_CheckedChanged);
            // 
            // panelOutput
            // 
            this.panelOutput.Controls.Add(this.listBoxStatus);
            this.panelOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelOutput.Location = new System.Drawing.Point(0, 347);
            this.panelOutput.Name = "panelOutput";
            this.panelOutput.Padding = new System.Windows.Forms.Padding(12, 6, 12, 12);
            this.panelOutput.Size = new System.Drawing.Size(1120, 232);
            this.panelOutput.TabIndex = 11;
            // 
            // listBoxStatus
            // 
            this.listBoxStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxStatus.FormattingEnabled = true;
            this.listBoxStatus.ItemHeight = 20;
            this.listBoxStatus.Location = new System.Drawing.Point(12, 6);
            this.listBoxStatus.Name = "listBoxStatus";
            this.listBoxStatus.Size = new System.Drawing.Size(1096, 214);
            this.listBoxStatus.TabIndex = 0;
            // 
            // panelListings
            // 
            this.panelListings.Controls.Add(this.dataGridViewListings);
            this.panelListings.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelListings.Location = new System.Drawing.Point(0, 67);
            this.panelListings.Name = "panelListings";
            this.panelListings.Padding = new System.Windows.Forms.Padding(12, 0, 12, 6);
            this.panelListings.Size = new System.Drawing.Size(1120, 280);
            this.panelListings.TabIndex = 12;
            // 
            // dataGridViewListings
            // 
            this.dataGridViewListings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RecordedDate,
            this.ModifiedDate,
            this.ProgrammeName,
            this.Channel,
            this.IsHighDef,
            this.Filesize});
            this.dataGridViewListings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewListings.Location = new System.Drawing.Point(12, 0);
            this.dataGridViewListings.MultiSelect = false;
            this.dataGridViewListings.Name = "dataGridViewListings";
            this.dataGridViewListings.ReadOnly = true;
            this.dataGridViewListings.RowTemplate.Height = 28;
            this.dataGridViewListings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewListings.Size = new System.Drawing.Size(1096, 274);
            this.dataGridViewListings.TabIndex = 13;
            this.dataGridViewListings.DoubleClick += new System.EventHandler(this.dataGridViewListings_DoubleClick);
            // 
            // RecordedDate
            // 
            this.RecordedDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.RecordedDate.DataPropertyName = "RecordedDate";
            this.RecordedDate.HeaderText = "Recorded Date";
            this.RecordedDate.Name = "RecordedDate";
            this.RecordedDate.ReadOnly = true;
            this.RecordedDate.Width = 105;
            // 
            // ModifiedDate
            // 
            this.ModifiedDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ModifiedDate.DataPropertyName = "ModifiedDate";
            this.ModifiedDate.HeaderText = "Modified Date";
            this.ModifiedDate.Name = "ModifiedDate";
            this.ModifiedDate.ReadOnly = true;
            this.ModifiedDate.Width = 105;
            // 
            // ProgrammeName
            // 
            this.ProgrammeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ProgrammeName.DataPropertyName = "ProgrammeName";
            this.ProgrammeName.HeaderText = "ProgrammeName";
            this.ProgrammeName.Name = "ProgrammeName";
            this.ProgrammeName.ReadOnly = true;
            this.ProgrammeName.Width = 302;
            // 
            // Channel
            // 
            this.Channel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Channel.DataPropertyName = "TvChannel";
            this.Channel.HeaderText = "Channel";
            this.Channel.Name = "Channel";
            this.Channel.ReadOnly = true;
            this.Channel.Width = 70;
            // 
            // IsHighDef
            // 
            this.IsHighDef.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IsHighDef.DataPropertyName = "IsHighDef";
            this.IsHighDef.HeaderText = "HD?";
            this.IsHighDef.Name = "IsHighDef";
            this.IsHighDef.ReadOnly = true;
            this.IsHighDef.Width = 35;
            // 
            // Filesize
            // 
            this.Filesize.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Filesize.DataPropertyName = "FileSizeMB";
            this.Filesize.HeaderText = "Filesize";
            this.Filesize.Name = "Filesize";
            this.Filesize.ReadOnly = true;
            this.Filesize.Width = 70;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 347);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1120, 3);
            this.splitter1.TabIndex = 13;
            this.splitter1.TabStop = false;
            // 
            // timer
            // 
            this.timer.Interval = 6000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(998, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 20);
            this.label3.TabIndex = 20;
            this.label3.Text = "HD";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(998, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 20);
            this.label4.TabIndex = 21;
            this.label4.Text = "SD";
            // 
            // labelHD
            // 
            this.labelHD.AutoSize = true;
            this.labelHD.Location = new System.Drawing.Point(1037, 9);
            this.labelHD.Name = "labelHD";
            this.labelHD.Size = new System.Drawing.Size(14, 20);
            this.labelHD.TabIndex = 22;
            this.labelHD.Text = "-";
            // 
            // labelSD
            // 
            this.labelSD.AutoSize = true;
            this.labelSD.Location = new System.Drawing.Point(1037, 36);
            this.labelSD.Name = "labelSD";
            this.labelSD.Size = new System.Drawing.Size(14, 20);
            this.labelSD.TabIndex = 23;
            this.labelSD.Text = "-";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 579);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panelOutput);
            this.Controls.Add(this.panelListings);
            this.Controls.Add(this.panelButtons);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Robert\'s Automated Programme Downloader";
            this.panelButtons.ResumeLayout(false);
            this.panelButtons.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownInterval)).EndInit();
            this.panelOutput.ResumeLayout(false);
            this.panelListings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListings)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelButtons;
        private System.Windows.Forms.CheckBox checkBoxSafeFilesOnly;
        private System.Windows.Forms.Button buttonDownloadAll;
        private System.Windows.Forms.Button buttonDownload;
        private System.Windows.Forms.Button buttonGetListing;
        private System.Windows.Forms.Panel panelOutput;
        private System.Windows.Forms.ListBox listBoxStatus;
        private System.Windows.Forms.Panel panelListings;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.DataGridView dataGridViewListings;
        private System.Windows.Forms.Button buttonToggleTimer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownInterval;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModifiedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProgrammeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Channel;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsHighDef;
        private System.Windows.Forms.DataGridViewTextBoxColumn Filesize;
        private System.Windows.Forms.Label labelSD;
        private System.Windows.Forms.Label labelHD;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}

