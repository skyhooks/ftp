﻿using FluentFTP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FtpDesktop
{
    public class FtpUtils
    {
        private string _ftpServer;
        private string _userName;
        private string _password;

        private FtpUtils() { }

        public FtpUtils(string ftpServer, string userName, string password)
        {
            _ftpServer = ftpServer;
            _userName = userName;
            _password = password;
        }

        public BindingSource GetRemoteListing(ref string errorMessage)
        {
            BindingSource fileList = new BindingSource();

            using (FtpClient client = new FtpClient(_ftpServer))
            {
                client.Credentials = new NetworkCredential(_userName, _password);
                try
                {
                    client.Connect();
                    foreach (FtpListItem item in client.GetListing())
                    {
                        if (item.Type == FtpFileSystemObjectType.File)
                        {
                            RecordedProgramInfo recordedProgramInfo = new RecordedProgramInfo(item);
                            if (recordedProgramInfo.FileExtension == "wtv")
                            {
                                fileList.Add(recordedProgramInfo);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    if (e.InnerException != null && e.InnerException.Message != null)
                    {
                        errorMessage = e.InnerException.Message;
                    }
                    else
                    {
                        errorMessage = e.Message;
                    }
                    return null;
                }
                finally
                {
                    client.Disconnect();
                }
            }
            return fileList;
        }

        public bool DownloadFile(string filename, long fileSize, string downloadFolder, ref string errorMessage)
        {
            bool downloadedSuccessfully = false;
            using (FtpClient client = new FtpClient(_ftpServer))
            {
                client.Credentials = new NetworkCredential(_userName, _password);
                try
                {
                    client.DataConnectionReadTimeout = 60000;
                    client.ReadTimeout = 60000;
                    client.RetryAttempts = 3;
                    client.Connect();

                    string tempFile = $"{downloadFolder}{filename}.tmp";

                    try
                    {
                        downloadedSuccessfully = client.DownloadFile(tempFile, filename, true, verifyOptions: FtpVerify.Throw & FtpVerify.Retry);
                    }
                    catch (Exception e)
                    {
                        if (e.InnerException != null &&
                            e.InnerException.Message != null &&
                            e.InnerException.Message == "Timed out trying to read data from the socket stream!" &&
                            new FileInfo(tempFile).Length == fileSize)
                        {
                            downloadedSuccessfully = true;
                        }
                        else
                        {
                            throw;
                        }
                    }

                    if (downloadedSuccessfully)
                    {
                        if (File.Exists($"{downloadFolder}{filename}"))
                        {
                            File.Delete($"{downloadFolder}{filename}");
                        }
                        File.Move(tempFile, $"{downloadFolder}{filename}");
                    }
                }
                catch (Exception e)
                {
                    if (e.InnerException != null && e.InnerException.Message != null)
                    {
                        errorMessage = e.InnerException.Message;
                    }
                    else
                    {
                        errorMessage = e.Message;
                    }
                    return false;
                }
                finally
                {
                    client.Disconnect();
                }
                return downloadedSuccessfully;
            }
        }
        public bool DownloadFile2(string filename, long fileSize, string downloadFolder, ref string errorMessage)
        {
            using (FtpClient client = new FtpClient(_ftpServer))
            {
                client.Credentials = new NetworkCredential(_userName, _password);
                client.Connect();

                using (Stream remoteFileStream = client.OpenRead(filename, FtpDataType.Binary))
                {
                    using (var localFileStream = File.Create($@"{downloadFolder}\{filename}"))
                    {
                        byte[] buffer = new byte[8 * 1024 * 1024];  // 8MB

                        int len;
                        while ((len = remoteFileStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            localFileStream.Write(buffer, 0, len);
                        }
                    }
                }

                FtpReply reply = client.GetReply();
                if (!reply.Success)
                {
                    return false;
                }
            }

            return true;
        }

        public bool MoveFile(string filename, string destinationFolder, ref string errorMessage)
        {
            using (FtpClient client = new FtpClient(_ftpServer))
            {
                client.Credentials = new NetworkCredential(_userName, _password);

                try
                {
                    client.DataConnectionReadTimeout = 60000;
                    client.ReadTimeout = 60000;
                    client.RetryAttempts = 3;
                    client.Connect();

                    if (client.FileExists(filename))
                    {
                        client.MoveFile(filename, $@"{destinationFolder}\{filename}");
                    }

                    return true;
                }
                catch (Exception e)
                {
                    errorMessage = e.Message;
                    return false;
                }
                finally
                {
                    client.Disconnect();
                }
            }
        }

    }
}
